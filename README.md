# Before Docker-Compose
* Please run `npm install` in `catalogue-service, frontend, product-service` folders
* The microservices directly connect to mongodb database hosted on cloud

# To start all the services
`docker-compose up -d`

# To stop all the services
`docker-compose stop`

# To get logs of frontend
`docker logs -f frontend`

# To get logs of catalogue-service
`docker logs -f catalogue-api`

# To get logs of product-service
`docker logs -f product-api`

# access web url 
`http://localhost:8080`

# access products api GET, POST
`http://localhost:3001/products`

# access products PUT, DELETE
`http://localhost:3001/products/:productId`

# access catalogue api GET, POST
`http://localhost:3000/categories`

# access catalogue api PUT, DELETE
`http://localhost:3000/categories/:categoryId`

# access productsbyCategory GET
`http://localhost:3000/productbycategory`


