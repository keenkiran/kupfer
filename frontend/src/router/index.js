import Vue from 'vue';
import Router from 'vue-router';

import Product from '../pages/Product.vue';
import Category from '../pages/Category.vue';
import Login from '../pages/core/Login.vue';
import Error from '../pages/core/Error.vue';
Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/products',
      name: 'Product',
      component: Product,
      meta: {
        breadcrumb: [
          { name: 'Product' }
        ]
      }
    },
    {
      path: '/categories',
      name: 'Category',
      component: Category,
      meta: {
        breadcrumb: [
          { name: 'Category' }
        ]
      }
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: {
        allowAnonymous: true
      }
    },
    {
      path: '/error',
      name: 'Error',
      component: Error,
      meta: {
        allowAnonymous: true
      }
    },
  ]
});
