// Core Components
import Toolbar from '../components/core/Toolbar.vue';
import Navigation from '../components/core/NavigationDrawer.vue';
import Breadcrumbs from '../components/core/Breadcrumbs.vue';
import PageFooter from '../components/core/PageFooter.vue';
import DataTable from '../components/DataTable.vue';
import ProductTable from '../components/ProductTable.vue';
import CategoryTable from '../components/CategoryTable.vue';

function setupComponents(Vue){
  Vue.component('toolbar', Toolbar);
  Vue.component('navigation', Navigation);
  Vue.component('breadcrumbs', Breadcrumbs);
  Vue.component('page-footer', PageFooter);
  Vue.component('data-table', DataTable);
  Vue.component('product-table', ProductTable);
  Vue.component('category-table', CategoryTable);
}

export {
  setupComponents
}
