const express = require('express')

const app = express()
const cors = require('cors');

const port = process.env.PORT || 3001

const mongoose = require('mongoose')

const uri = "mongodb+srv://root:root@dev-chdqu.mongodb.net/ProductDB?retryWrites=true&w=majority";

// eslint-disable-next-line no-unused-vars
const Task = require('./api/models/ProductModel')
// created model loading here

const bodyParser = require('body-parser')

// mongoose instance connection url connection
mongoose.Promise = global.Promise
mongoose.connect(uri, { auth: { authdb: 'admin' }, useNewUrlParser: true })

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

const routes = require('./api/routes/ProductRoutes') // importing route
routes(app) // register the route

app.listen(port)

console.log('Product API server started on: ' + port)
