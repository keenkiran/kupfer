'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ProductSchema = new Schema({
  name: {
    type: String,
    required: 'Kindly enter the name of the product'
  },
  price: {
    type: Number,
    required: 'Kindly enter the price of the product'
  },
  created_date: {
    type: Date,
    default: Date.now
  },
  categoryId: {
    type: [Schema.Types.ObjectId],
    required: 'Kindly enter the UUID of the product'
  }
})

module.exports = mongoose.model('Products', ProductSchema)
