'use strict'

module.exports = function (app) {
  const productCtrl = require('../controllers/ProductController')

  // productCtrl Routes
  app.route('/products')
    .get(productCtrl.listProducts)
    .post(productCtrl.createProduct)

  app.route('/products/:productId')
    .get(productCtrl.getProduct)
    .put(productCtrl.updateProduct)
    .delete(productCtrl.deleteProduct)
  
  app.route('/productsbycategory/:categoryId')
    .get(productCtrl.productsByCategory)  
}
