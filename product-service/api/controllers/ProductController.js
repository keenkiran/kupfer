'use strict'

const mongoose = require('mongoose')

const Product = mongoose.model('Products')

exports.listProducts = function (req, res) {
  Product.find({}, function (err, product) {
    if (err) { res.send(err) }
    res.json(product)
  })
}

exports.createProduct = function (req, res) {
  const newProduct = new Product(req.body)
  newProduct.save(function (err, product) {
    if (err) { res.send(err) }
    res.json(product)
  })
}

exports.getProduct = function (req, res) {
  product.findById(req.params.productId, function (err, product) {
    if (err) { res.send(err) }
    res.json(product)
  })
}

exports.updateProduct = function (req, res) {
  Product.findOneAndUpdate({ _id: req.params.productId }, req.body, { new: true }, function (err, product) {
    if (err) { res.send(err) }
    res.json(product)
  })
}

exports.deleteProduct = function (req, res) {
  Product.remove({
    _id: req.params.productId
  }, function (err, product) {
    if (err) { res.send(err) }
    res.json({ message: 'product successfully deleted' })
  })
}

exports.productsByCategory = function (req, res) {
  let categoryId = req.params.categoryId
  Product.find({
    'categoryId': { $in: [
        mongoose.Types.ObjectId(categoryId)
    ]}
}, function(err, docs){
    if (err) { res.send(err) }
    res.json(docs);
});
} 
