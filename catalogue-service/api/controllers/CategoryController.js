'use strict'

const mongoose = require('mongoose')

const Category = mongoose.model('Categories')

exports.listCategories = function (req, res) {
  Category.find({}, function (err, category) {
    if (err) { res.send(err) }
    res.json(category)
  })
}

exports.createCategory = function (req, res) {
  const newCategory = new Category(req.body)
  newCategory.save(function (err, category) {
    if (err) { res.send(err) }
    res.json(category)
  })
}

exports.getCategory = function (req, res) {
  Category.findById(req.params.categoryId, function (err, category) {
    if (err) { res.send(err) }
    res.json(category)
  })
}

exports.updateCategory = function (req, res) {
  Category.findOneAndUpdate({ _id: req.params.categoryId }, req.body, { new: true }, function (err, category) {
    if (err) { res.send(err) }
    res.json(category)
  })
}

exports.deleteCategory = function (req, res) {
  Category.remove({
    _id: req.params.categoryId
  }, function (err, category) {
    if (err) { res.send(err) }
    res.json({ message: 'Category successfully deleted' })
  })
}
