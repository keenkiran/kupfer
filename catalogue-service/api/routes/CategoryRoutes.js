'use strict'

module.exports = function (app) {
  const categoryCtrl = require('../controllers/CategoryController')

  // categoryCtrl Routes
  app.route('/categories')
    .get(categoryCtrl.listCategories)
    .post(categoryCtrl.createCategory)

  app.route('/categories/:categoryId')
    .get(categoryCtrl.getCategory)
    .put(categoryCtrl.updateCategory)
    .delete(categoryCtrl.deleteCategory)
}
