const express = require('express')

const app = express()
const cors = require('cors');

const port = process.env.PORT || 3000

const mongoose = require('mongoose')

const uri = "mongodb+srv://root:root@dev-chdqu.mongodb.net/CategoryDB?retryWrites=true&w=majority";

// eslint-disable-next-line no-unused-vars
const Category = require('./api/models/categoryModel')

// created model loading here

const bodyParser = require('body-parser')

// mongoose instance connection url connection //
mongoose.Promise = global.Promise
mongoose.connect(uri, { auth: { authdb: 'admin' }, useNewUrlParser: true })

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

const routes = require('./api/routes/CategoryRoutes') // importing route
routes(app) // register the route

app.listen(port)

console.log('catalogue API server started on: ' + port)
